import sys

# длина кругового массива
n = int(sys.argv[1])

# длина интервала
m = int(sys.argv[2])

# конец счёта - первый элемент кругового масива (индекс 1)
# interval - список интервалов откуда возьмём первые значения для ответа
# path - результирущее число, преобразованное из строки

wheel_str = ''.join(str(x) for x in range(1, n + 1))
wheel = wheel_str
count = 0
interval_list = []


while True:
    interval = wheel[count:count+m]

    if len(interval) < m:
        wheel += wheel_str
        continue

    count += m - 1
    interval_list.append(interval)

    if interval[-1:] == wheel[0]:
        break

path = ''.join(i[0] for i in interval_list)
print(path)


# 12345  - len = 5
# 1234, 4512, 2345, 5123, 3451 

# 123456
# 12, 23, 34, 45, 56, 61