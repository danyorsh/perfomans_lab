import math
import sys


def get_result():
    file_circle_path = sys.argv[1]
    file_point_path = sys.argv[2]
    result = ''

    with open(file_circle_path, 'r') as file:
        data = file.readline().split()
        try:
            cir_x, cir_y = int(data[0]), int(data[1])
        except IndexError as exc:
            print('Ожидалось получить два числа (координата x и координата у).', exc)
            return

        rad = int(file.readline()[0])

    with open(file_point_path, 'r') as file:
        data = file.readlines()
        for coordinates in data:
            try:
                point_x = int(coordinates.split()[0])
                point_y = int(coordinates.split()[1])
            except ValueError as exc:
                print('Ожидалось получить числовые значения.', exc)
                continue
            except IndexError as exc:
                print('Ожидалось получить два числа (координата x и координата у).', exc)
                continue
            result += math_calc(cir_x, cir_y, rad, point_x, point_y) + '\n'

    print(result)


def math_calc(circle_x, circle_y, radius, point_x, point_y):

    path = math.sqrt((point_x - circle_x) ** 2 + (point_y - circle_y) ** 2)

    if path > radius:
        return '2'
    elif path < radius:
        return '1'
    else:
        return '0'


if __name__ == '__main__':
    get_result()