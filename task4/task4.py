import statistics
import sys
from statistics import median


def min_moves():
    numbers_file = sys.argv[1]
    try:
        with open(numbers_file, 'r') as file:

            numbers = [int(num[:-1]) for num in file.readlines()]
            median_num = median(numbers)
            res = sum(abs(num - median_num) for num in numbers)
            print(int(res))

    except FileNotFoundError as exc:
        print('Файла не существует, проверьте путь.', exc)
    except statistics.StatisticsError:
        print('Файл пуст')
    except ValueError as exc:
        print('Ожидалось получить числа. Проверьте файл.', exc)


if __name__ == '__main__':
    min_moves()