import sys
import json


values_file = sys.argv[1]
tests_file = sys.argv[2]
report_file = sys.argv[3]
REPORT_DATA = {'tests': []}


def get_value(id_num):
    """ Функция поиска значений по id. """

    with open(values_file) as file:
        values_dict = json.load(file)['values']
        for i in values_dict:
            if i['id'] == id_num:
                return i['value']


def find_path(data):
    """ Рекурсивная функция прохождения структуры. """

    if isinstance(data, list):
        for elem in data:
            elem['value'] = get_value(elem['id'])

            if elem.get('values'):
                find_path(elem['values'])

    if isinstance(data, dict):
        if data.get('values'):
            find_path(data['values'])

        for key, value in data.items():

            if isinstance(value, list):
                for elem in value:
                    elem['value'] = get_value(elem['id'])

                    if elem.get('values'):
                        find_path(elem['values'])

                    REPORT_DATA['tests'].append(elem)


if __name__ == '__main__':

    with open(tests_file) as file:
        d_dict = json.load(file)

    find_path(d_dict)

    with open(report_file, 'w') as file:
        json.dump(REPORT_DATA, file)
